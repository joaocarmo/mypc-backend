FROM node:16-alpine AS builder

WORKDIR /opt/mypc

COPY package.json yarn.lock ./

RUN yarn --frozen-lockfile

# Use npm modules with binaries
ENV PATH /opt/mypc/node_modules/.bin:$PATH

COPY . .

FROM builder AS worker

# Set the server port
ENV SERVER_PORT 3001

CMD [ "yarn", "start" ]
