# mypc-backend

Backend POC using [Feathers](https://docs.feathersjs.com).

## API

There is a simple `/messages` API with methods to `get`, `create` and `delete`.

```txt
# Get all messages
GET /messages

# Get message by ID
GET /messages/1

# Create new message
POST /messages

# Delete message by ID
DELETE /messages/1
```

## Deployment

```sh
# Build docker image (tag: mypc-backend)
docker build -t mypc-backend .

# Start one instance of the docker image
docker run -p 3001:3001 mypc-backend
```

The app will be available at <http://localhost:3001> and is self-contained.
